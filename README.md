# TP_QA

### Запуск Selenium

Качаем по ссылкам

* Бинарник selenium (selenium-server-standalone-3.11.0.jar): http://selenium-release.storage.googleapis.com/index.html?path=3.11/
* Бинарник geckodriver: https://github.com/mozilla/geckodriver/releases
* Бинарник chromedriver: http://chromedriver.storage.googleapis.com/index.html?path=2.38/

Помещаем файлы в директорию и в ней запускаеи хаб и ноду

Запукаем хаб
```
sudo java -jar selenium-server-standalone-3.11.0.jar -role hub
```

Запускаем ноду
```
sudo java -Dwebdriver.chrome.driver="./chromedriver" -Dwebdriver.gecko.driver="./geckodriver" -jar selenium-server-standalone-3.11.0.jar -role node -hub http://localhost:4444/grid/register -browser browserName=chrome,maxInstances=2 -browser browserName=firefox,maxInstances=2
```
