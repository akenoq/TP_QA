# -*- coding: utf-8 -*-

import os

import unittest
from time import sleep

from page_objects import AuthPage, PhotoPage

from selenium.webdriver import DesiredCapabilities, Remote


class Test(unittest.TestCase):
    USERNAME = u'Куклина Нина'
    # LOGIN = os.environ.get('LOGIN')
    # PASSWORD = os.environ.get('PASSWORD')
    LOGIN = 'technopark25'
    PASSWORD = 'testQA1'

    def setUp(self):
        browser = os.environ.get('BROWSER', 'CHROME')

        self.driver = Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            desired_capabilities=getattr(DesiredCapabilities, browser).copy()
        )

        # self.driver.implicitly_wait(3)

        auth_page = AuthPage(self.driver)
        auth_page.open()

        auth_form = auth_page.form
        auth_form.set_login(self.LOGIN)
        auth_form.set_password(self.PASSWORD)
        auth_form.submit()

        self.photo_page = PhotoPage(self.driver)

    def tearDown(self):
        self.driver.quit()

    # OK F + NORM
    # def test_answer_comment(self):
    #     answer_text = 'answer'
    #     self.photo_page.goto_photo_comment()
    #     comments = self.photo_page.comments
    #
    #     comments.click_answer()
    #
    #     input_comment = self.photo_page.input_comment
    #     input_comment.add_answer_text(answer_text)
    #
    #     ans = comments.check_answer()
    #     print ans
    #     self.assertTrue(ans)

    # IndexError: list index out of range
    # lambda d: d.find_elements_by_css_selector(css)[0]
    # OK ok E + NORM
    # def test_add_video(self):
    #     self.photo_page.goto_photo_comment()
    #     input_comment = self.photo_page.input_comment
    #
    #     input_comment.add_video()
    #
    #     input_comment.send()
    #
    #     comments = self.photo_page.comments
    #     video_in_attach_number = comments.get_newest_comment_video_attach_num()
    #     self.assertEqual(1, video_in_attach_number)

    # lambda d: d.find_element_by_xpath(self.SUBMIT_BTN)
    # ElementNotInteractableException: Message: Element <button class="button-pro form-actions_yes"> could not be scrolled into view
    # на ФОТО
    # OK ok E + NORM
    # def test_add_one_photo(self):
    #     self.photo_page.goto_photo_comment()
    #     input_comment = self.photo_page.input_comment
    #
    #     input_comment.add_one_photo()
    #     input_comment.wait_progress_bar()
    #     input_comment.send()
    #
    #     comments = self.photo_page.comments
    #     photo_in_attach_number = comments.get_newest_comment_photo_attach_num()
    #
    #     self.assertEqual(1, photo_in_attach_number)

    # # OK ok E + NORM
    # def test_add_two_photo(self):
    #     self.photo_page.goto_photo_comment()
    #     input_comment = self.photo_page.input_comment
    #
    #     input_comment.add_one_photo()
    #     input_comment.wait_progress_bar()
    #
    #     input_comment.add_one_photo()
    #     input_comment.wait_progress_bar()
    #
    #     input_comment.send()
    #
    #     comments = self.photo_page.comments
    #     photo_in_attach_number = comments.get_newest_comment_photo_attach_num()
    #
    #     self.assertEqual(2, photo_in_attach_number)

    # # OK ok OK 5 NORM
    # def test_add_comment_sticker(self):
    #     self.photo_page.goto_photo_comment()
    #
    #     input_comment = self.photo_page.input_comment
    #     sticker_data_code = input_comment.add_comment_sticker()
    #
    #     self.photo_page.reload()
    #
    #     comments = self.photo_page.comments
    #     newest_sticker_data_code = comments.get_newest_comment_sticker()
    #     print 'content = ', newest_sticker_data_code
    #     self.assertEqual(sticker_data_code, newest_sticker_data_code)

    # # OK ok + norm
    # def test_add_comment_smile(self):
    #     smile_class = "emoji_ok_04"
    #     self.photo_page.goto_photo_comment()
    #
    #     input_comment = self.photo_page.input_comment
    #     input_comment.add_comment_smile(smile_class)
    #
    #     self.photo_page.reload()
    #
    #     comments = self.photo_page.comments
    #     content = comments.get_newest_comment_smile()
    #     print 'content = ', content
    #     self.assertIn(smile_class, content)

    # # # fail... True != False ok
    def test_add_klass(self):
        self.photo_page.goto_photo_comment()
        comments = self.photo_page.comments

        comments.add_klass()

        klass = comments.check_klass()
        self.assertTrue(klass)

    # # OK ok + norm
    # def test_add_comment(self):
    #     text = 'hello QA'
    #     self.photo_page.goto_photo_comment()
    #
    #     input_comment = self.photo_page.input_comment
    #     input_comment.add_comment_text(text)
    #
    #     comment_text = self.photo_page.comments.get_newest_comment_text()
    #     self.assertEqual(text, comment_text)

    # # ElementNotInteractableException: Message: Element <button class="button-pro comments_add-controls_save"> could not be scrolled into view
    # # OK fix тоже фэилит периодически
    # def test_edit_comment(self):
    #     fixed_text = '_fixed'
    #     self.photo_page.goto_photo_comment()
    #
    #     comments = self.photo_page.comments
    #     origin_text = comments.get_newest_comment_text()
    #
    #     comments.edit_comment(fixed_text)
    #
    #     comments_new = self.photo_page.comments
    #     edited_text = comments_new.get_newest_comment_text()
    #
    #     print 'edited = ', edited_text
    #     self.assertEqual(origin_text+fixed_text, edited_text)

    # # OK редко fail...
    # # SAVE_BTN ElementNotInteractableException: Message: Element <button class="button-pro comments_add-controls_save"> could not be scrolled into view
    # def test_edit_comment_to_empty(self):
    #     self.photo_page.goto_photo_comment()
    #
    #     comments = self.photo_page.comments
    #     origin_text = comments.get_newest_comment_text()
    #
    #     comments.clear_comment()
    #
    #     self.photo_page.reload()
    #
    #     comments_new = self.photo_page.comments
    #     edited_text = comments_new.get_newest_comment_text()
    #
    #     print 'edited = ', edited_text
    #     self.assertEqual(origin_text, edited_text)

    # # OK fix NORM
    # # hover
    # # NoSuchElementException: Message: Unable to locate element: //a[contains(@class, "delete-stub_cancel")]
    # def test_undelete_comment(self):
    #     self.photo_page.goto_photo_comment()
    #
    #     comments = self.photo_page.comments
    #
    #     count_first = comments.count_comments()
    #
    #     comments.delete_comment()
    #     # sleep(1)
    #     comments.undelete_comment()
    #     self.photo_page.reload()
    #
    #     count_second = comments.count_comments()
    #     delta = count_first - count_second
    #     print 'delta = ', delta
    #     self.assertEqual(delta, 0)

    # #  OK fix ok NORM
    # def test_delete_comment(self):
    #     self.photo_page.goto_photo_comment()
    #
    #     comments = self.photo_page.comments
    #
    #     count_first = comments.count_comments()
    #
    #     comments.delete_comment()
    #     self.photo_page.reload()
    #
    #     count_second = comments.count_comments()
    #     delta = count_first - count_second
    #     print 'delta = ', delta
    #     self.assertNotEqual(delta, 0)
