# -*- coding: utf-8 -*-

import os

import urlparse

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchElementException

import time


# ИМПОРТЫ

class Page(object):
    BASE_URL = 'https://ok.ru/'
    PATH = ''

    def __init__(self, driver):
        self.driver = driver

    def open(self):
        url = urlparse.urljoin(self.BASE_URL, self.PATH)
        self.driver.get(url)
        self.driver.maximize_window()

    def redirect(self, path):
        url = urlparse.urljoin(self.BASE_URL, path)
        self.driver.get(url)

    def reload(self):
        self.driver.refresh()


class AuthPage(Page):
    @property
    def form(self):
        return AuthForm(self.driver)


class PhotoPage(Page):
    PHOTO = '//a[@class="card_wrp"]'

    def goto_photo_comment(self):
        try:
            self.redirect('kadyr.akhmatov/pphotos/865862332740')
            # WebDriverWait(self.driver, 30, 0.1).until(
            #     EC.element_to_be_clickable((By.XPATH, self.PHOTO))
            # ).click()
            progress_roll = '.photo-layer_process'
            WebDriverWait(self.driver, 30, 0.1).until(
                EC.invisibility_of_element_located((By.CSS_SELECTOR, progress_roll))
            )
        except TimeoutException:
            print 'page is not loaded'
            return 0
            # self.driver.find_element_by_xpath(self.PHOTO).click()

    @property
    def comments(self):
        return Comments(self.driver)

    @property
    def input_comment(self):
        return InputComment(self.driver)


class Component(object):
    def __init__(self, driver):
        self.driver = driver

    def hover(self, element_xpath):
        WebDriverWait(self.driver, 20, 0.1).until(
            EC.presence_of_element_located((By.XPATH, element_xpath))
        )
        element = self.driver.find_element_by_xpath(element_xpath)

        from selenium.webdriver import ActionChains
        hov = ActionChains(self.driver).move_to_element(element)
        hov.perform()
        # try:
        #     WebDriverWait(self.driver, 20, 0.1).until(
        #         EC.presence_of_element_located((By.XPATH, element_xpath))
        #     )
        #     element = self.driver.find_element_by_xpath(element_xpath)
        #     from selenium.webdriver import ActionChains
        #     hov = ActionChains(self.driver).move_to_element(element)
        #     hov.perform()
        # except TimeoutException:
        #     return 0

    def hover_css(self, element_css):
        try:
            WebDriverWait(self.driver, 20, 0.1).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, element_css))
            )
            element = self.driver.find_element_by_css_selector(element_css)
            from selenium.webdriver import ActionChains
            hov = ActionChains(self.driver).move_to_element(element)
            hov.perform()
        except TimeoutException:
            return 0


class Comments(Component):
    COMMENT = '//div[contains(@class, "comments_text")]'
    COMMENT_BOX = './/div[@class="comments_i"]'

    COMMENT_LIST_CSS_lastchild = '.comments_lst_cnt > div[id^="hook_Block"]:last-child'
    ANS_BTN_CSS_lastchild = '.comments_lst_cnt > div[id^="hook_Block"]:last-child .reply_w'

    COMMENT_BOX_CSS_last = '.comments_lst_cnt > div[id^="hook_Block"]:last-child div[class="comments_i"]'

    EDIT_BTN = '//a[contains(@class, "comments_edit")]'
    EDIT_INPUT = '//div[contains(@class, "comments_add-ceditable")]'
    SAVE_BTN = '//button[contains(@class, "comments_add-controls_save")]'

    DELETE_BTN = '//a[contains(@class, "comments_remove")]'
    UNDELETE_BTN = '//a[contains(@class, "delete-stub_cancel")]'

    # KLASS_BTN = '//div[contains(@class, "al") and contains(@class, "controls-list_lk")]'
    # contains( @class ,"title") and contains( @ class, "title_size_32")
    # KLASS_BTN = '//div[@class="klass_w"]'
    KLASS_BTN = 'ic_klass-o'

    ANSWER_BTN = '//div[contains(@class, "reply_w")]'

    NUM_COMMENTS = ".photo-layer_bottom_block_w .widget.__no-o a[data-module='CommentWidgets'] .widget_count.js-count"

    # скролл страницы
    # def scroll_down(self):
    #     self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    #
    # def scroll_start(self):
    #     return self.driver.execute_script("return window.pageYOffset;")

    def get_newest_comment_text(self):
        try:
            comment_text_field = self.driver.find_elements_by_xpath(self.COMMENT)[-1]
            print comment_text_field.text
            return comment_text_field.text
        except IndexError:
            print 'wasnt loaded comments'
            return 0

    # smile
    def get_newest_comment_smile(self):
        # COMMENT_CONTENT = self.COMMENT + '/*/'
        COMMENT_SMILE = self.COMMENT + '/*/img'
        print 'COMMENT SMILE = ', COMMENT_SMILE
        try:
            comment_field = self.driver.find_elements_by_xpath(COMMENT_SMILE)[-1]
            print 'CONT = ', comment_field.get_attribute("class")
            return comment_field.get_attribute("class").split(' ')
        except NoSuchElementException:
            print 'not found element'
            return 0
        except IndexError:
            print 'wasnt loaded comments'
            return 0

    # sticker
    def get_newest_comment_sticker(self):
        COMMENT_STICKER = self.COMMENT + '/*/div[contains(@class, "__sticker")]'
        print 'COMMENT SMILE = ', COMMENT_STICKER
        try:
            comment_field = self.driver.find_elements_by_xpath(COMMENT_STICKER)[-1]
            data_code = comment_field.get_attribute("data-code")
            return data_code
        except NoSuchElementException:
            print 'not found element'
            return 0
        except IndexError:
            print 'wasnt loaded comments'
            return 0

            # sticker

    def get_newest_comment(self):
        try:
            # return self.driver.find_element_by_css_selector(self.COMMENT_LIST_CSS_lastchild)

            # WebDriverWait(self.driver, 10, 0.1).until(
            #     EC.element_to_be_clickable((By.XPATH, self.SAVE_BTN))
            # )
            try:
                elem = WebDriverWait(self.driver, 30, 0.1).until(
                    lambda d: d.find_element_by_css_selector(self.COMMENT_LIST_CSS_lastchild)
                )
                return elem
            except TimeoutException:
                print 'cant get_newest_comment'
                return 0
        except NoSuchElementException:
            print 'not found element'
            return 0

    def get_newest_comment_photo_attach_num(self):
        # COMMENT_IMG_CSS = 'div[data-attaches*=\'[{\"type\":\"PHOTOODKL")]'
        # try:
        #     # сразу все картинки
        #     el = self.get_newest_comment()
        #     print 'HERE LEN = ', el
        #     el = el.find_elements_by_css_selector('.collage_i')
        #     print el
        #     print len(el)
        #     return len(el)
        # except NoSuchElementException:
        #     print 'not found element'
        #     return 0
        # except IndexError:
        #     print 'wasnt loaded comments'
        #     return 0
        el = self.get_newest_comment()
        print 'HERE LEN = ', el
        el = el.find_elements_by_css_selector('.collage_i')
        print el
        print len(el)
        return len(el)

    def get_newest_comment_video_attach_num(self):
        # document.querySelectorAll('.comments_lst_cnt > div[id^="hook_Block"]:last-child div[data-l="t,play"]')
        VIDEO_CONT_CSS = 'div[data-l="t,play"]'
        try:
            # сразу все элементы
            el = self.get_newest_comment()
            print 'HERE LEN = ', el
            el = el.find_elements_by_css_selector(VIDEO_CONT_CSS)
            print el
            print len(el)
            return len(el)
        except NoSuchElementException:
            print 'not found element'
            return 0
        except IndexError:
            print 'wasnt loaded comments'
            return 0

        ########################################################################################################################

    def input_text(self, element, text):
        WebDriverWait(self.driver, 30, 0.1).until(
            lambda d: d.find_element_by_xpath(element)
        ).send_keys(text)

    def save(self):
        self.driver.execute_script("arguments[0].scrollIntoView(true);",
                                   self.driver.find_element_by_xpath(self.SAVE_BTN))
        # WebDriverWait(self.driver, 30, 0.1).until(
        #     lambda d: d.find_element_by_xpath(self.SAVE_BTN)
        # ).click()
        WebDriverWait(self.driver, 20, 0.1).until(
            EC.element_to_be_clickable((By.XPATH, self.SAVE_BTN))
        ).click()
        # WebDriverWait(self.driver, 10, 0.1).until(EC.element_to_be_clickable((By.XPATH, self.SAVE_BTN))).click()

    def click_edit(self):
        try:
            self.driver.execute_script("arguments[0].scrollIntoView(true);", self.get_newest_comment())

            self.driver.execute_script("arguments[0].scrollIntoView(true);",
                                       self.driver.find_elements_by_xpath(self.EDIT_BTN)[-1])
            self.hover_css(self.COMMENT_LIST_CSS_lastchild + ' a.comments_edit')
            # time.sleep(1)
            # WebDriverWait(self.driver, 30, 0.1).until(
            #             lambda d: d.find_elements_by_xpath(self.EDIT_BTN)[-1]
            #         ).click()
            print 'CLICK'
            self.driver.execute_script("arguments[0].click()",
                                       self.driver.find_elements_by_xpath(self.EDIT_BTN)[-1])
        except IndexError:
            print 'wasnt loaded comments'
            return 0

    def edit_comment(self, fixed_text):
        # push edit btn
        self.click_edit()
        # input text
        self.input_text(self.EDIT_INPUT, fixed_text)
        self.save()

    ########################################################################################################################

    def click_css(self, css):
        WebDriverWait(self.driver, 20, 0.1).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, css))
        ).click()

    def click_xpath(self, xpath):
        WebDriverWait(self.driver, 20, 0.1).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, xpath))
        ).click()

    def click_answer(self):
        # UP
        # BTN_UP = '.loader-controls_scroll.__up'
        # # self.driver.execute_script("document.querySelectorAll('.loader-controls_scroll.__up')[0].click()")
        # WebDriverWait(self.driver, 20, 0.1).until(
        #     EC.element_to_be_clickable((By.CSS_SELECTOR, BTN_UP))
        # ).click()

        answer_btn = '//div[contains(@class, "reply_w")]'

        # self.hover(self.COMMENT_BOX)
        # MY HOVER
        # document.querySelectorAll('.comments_reply.al.tdn.fade-on-hover')[0].className = "comments_reply al tdn"
        self.driver.execute_script('document.querySelectorAll(\'.comments_reply.al.tdn.fade-on-hover\')[0].className = "comments_reply al tdn"')

        # Комментарий добавлен КНОПКА ВНИЗ
        # активировать ии кликнуть
        # "tip __bot inverted tipDiscussion __active"


        # ЖМУ НА КНОПКУ
        new_selector_ans_btn = '.comments_reply.al.tdn'
        WebDriverWait(self.driver, 20, 0.1).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, new_selector_ans_btn))
        ).click()

        # WebDriverWait(self.driver, 20, 0.1).until(
        #     EC.element_to_be_clickable((By.XPATH, answer_btn))
        # ).click()

        # NEWEST COM
        # self.driver.execute_script("document.querySelectorAll('.comments_lst_cnt > div[id^=\"hook_Block\"]:last-child')[0].scrollIntoView(true)")
        #
        # self.hover(self.COMMENT_BOX)
        #
        # WebDriverWait(self.driver, 20, 0.1).until(
        #     EC.element_to_be_clickable((By.XPATH, self.ANS_BTN_CSS_lastchild))
        # ).click()



        # try:
        #     self.hover(self.COMMENT_BOX)
        #     WebDriverWait(self.driver, 30, 0.1).until(
        #         lambda d: d.find_element_by_xpath(answer_btn)
        #     ).click()
        # except IndexError:
        #     print 'wasnt loaded comments'
        #     return 0



    def clear_comment(self):
        self.click_edit()
        self.driver.find_element_by_xpath(self.EDIT_INPUT).clear()
        # time.sleep(1)
        self.save()

    ########################################################################################################################

    def click_klass(self):
        # push like
        try:
            # HOVER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            # self.hover(self.COMMENT_BOX)

            self.driver.execute_script('document.querySelectorAll(\'.al.fade-on-hover.controls-list_lk\')[0].className = "al controls-list_lk"')

            WebDriverWait(self.driver, 10, 0.1).until(
                EC.element_to_be_clickable((By.CLASS_NAME, self.KLASS_BTN))
            ).click()
            # WebDriverWait(self.driver, 30, 0.1).until(
            #     lambda d: d.find_element_by_class_name(self.KLASS_BTN)
            # ).click()
        except IndexError:
            print 'wasnt loaded comments'
            return 0

    def check_klass(self):
        # KLASS_TEXT = '"' + 'al  controls-list_lk' + '"'
        # get_inner_text_script = 'return document.getElementsByClassName(' + KLASS_TEXT + ')[0].innerText;'
        # text = self.driver.execute_script(get_inner_text_script)

        klass_marker_css = '.al.controls-list_lk'
        # wait sticker
        try:
            WebDriverWait(self.driver, 20, 0.1).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, klass_marker_css))
            )
            # lambda на появление атрибута
            marker_data_o_id = 'span.al.controls-list_lk[data-o-id*="A"]'
            WebDriverWait(self.driver, 10, 0.1).until(
                lambda d: d.find_elements_by_css_selector('span.controls-list_lk[data-o-id*="A"]')
            )

            text = self.driver.find_element_by_css_selector(klass_marker_css).get_attribute("innerText")
            # return marker
        except Exception:
            return 0

        print "TEXT = ", text

        klassed_text = "Вы".decode('utf-8')

        self.del_klass()

        # переместить в тест
        if text == klassed_text:
            return True
        else:
            return False

    def add_klass(self):
        # set klass
        self.click_klass()
        print 'KLASS ADD'

    def del_klass(self):
        # unset klass
        self.click_klass()
        print 'KLASS DEL'

    ########################################################################################################################

    def check_answer(self):
        # поиск в последнем комменте
        ANS_MARKER_CSS = '.comments_lst_cnt > div[id^="hook_Block"]:last-child .vaTop'

        # marker = self.driver.find_element_by_css_selector(ANS_MARKER_CSS).get_attribute("innerText")
        WebDriverWait(self.driver, 10, 0.1).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, ANS_MARKER_CSS))
        )
        # marker = self.driver.execute_script("return document.querySelectorAll('.comments_lst_cnt > div[id^=\"hook_Block\"]:last-child .vaTop')[0].innerText")
        marker = self.driver.find_element_by_css_selector(".comments_lst_cnt > div[id^=\"hook_Block\"]:last-child .vaTop").text
        return marker

    def count_comments(self):
        # count = len(self.driver.find_elements_by_xpath(self.COMMENT_BOX))
        NUM_COMMENTS = ".photo-layer_bottom_block_w .widget.__no-o a[data-module='CommentWidgets'] .widget_count.js-count"
        count = int(self.driver.find_element_by_css_selector(NUM_COMMENTS).text)
        print 'COUNT = ', count
        return count

    def delete_comment(self):
        # time.sleep(1)
        # self.driver.execute_script("document.getElementsByClassName('comments_remove')[0].click()")
        try:
            # time.sleep(5)
            # self.driver.execute_script("document.querySelectorAll('.loader-controls_scroll.__up')[0].click()")



            # self.hover(self.COMMENT_BOX)
            self.driver.execute_script(
                'document.querySelectorAll(\'.fade-on-hover.comments_remove.ic10.ic10_close-g\')[0].className = "comments_remove ic10 ic10_close-g"')
            print 'EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE'

            # ЧИСЛО КОМ
            n = self.driver.find_element_by_css_selector(self.NUM_COMMENTS).text

            # НЕ БУДЕТ CLICKABLE, если не прокрутить наверх
            WebDriverWait(self.driver, 10, 0.1).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.comments_remove'))
            ).click()

            # ЧИСЛО КОМ
            num_after = str(int(n) - 1)
            WebDriverWait(self.driver, 10, 0.1).until(
                EC.text_to_be_present_in_element((By.CSS_SELECTOR, self.NUM_COMMENTS), num_after)
            )

            deleted_marker_css = '.comments_i.__deleted'
            # self.driver.find_element_by_xpath(self.DELETE_BTN).click()
            WebDriverWait(self.driver, 10, 0.1).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, deleted_marker_css))
            )

        except NoSuchElementException:
            return 0
            # WebDriverWait(self.driver, 30, 0.1).until(
            #     lambda d: d.find_element_by_xpath(self.DELETE_BTN)
            # ).click()
            # time.sleep(1)

    def undelete_comment(self):
        # self.hover(self.COMMENT_BOX)
        try:
            # self.driver.find_element_by_xpath(self.UNDELETE_BTN).click()
            WebDriverWait(self.driver, 20, 0.1).until(
                EC.element_to_be_clickable((By.XPATH, self.UNDELETE_BTN))
            ).click()
        except NoSuchElementException:
            return 0
            # WebDriverWait(self.driver, 10, 0.1).until(
            #     EC.element_to_be_clickable((By.XPATH, self.UNDELETE_BTN))
            # ).click()
            # # self.driver.find_element_by_xpath(self.UNDELETE_BTN).click()


def wait_until_send_comment(decorated_method):
    def wrapper(self):
        NUM_COMMENTS = ".photo-layer_bottom_block_w .widget.__no-o a[data-module='CommentWidgets'] .widget_count.js-count"
        n = self.driver.find_element_by_css_selector(NUM_COMMENTS).text
        decorated_method()
        num_after = str(int(n) + 1)
        WebDriverWait(self.driver, 20, 0.1).until(
            EC.text_to_be_present_in_element((By.CSS_SELECTOR, NUM_COMMENTS), num_after)
        )


class InputComment(Component):
    INPUT = '//div[@name="st.dM"]'
    SUBMIT_BTN = '//div[@class="comments_add-controls"]/button[@data-l="t,submit"]'
    SAVE_BTN = '//button[contains(@class, "comments_add-controls_save")]'

    SMILE_BTN = '//span[contains(@class, "comments_smiles_trigger")]'
    # SMILE_BTN = '//span[@data-l="t,smiles"]'
    SMILE_POPUP = '//span[contains(@class, "comments_smiles_popup")]'
    SMILE_TAB = '//a[@data-l="t,smilesTab"]'

    STICKER_TAB = '//a[@data-l="t,stickersTab"]'
    OK_STICKERS_TAB = '//a[@class="comments_smiles_nav_a" and @href="#collection_nav_id_set.ok"]'

    ATTACH_BTN = '//span[contains(@class, "comments_attach_trigger")]'

    NUM_COMMENTS = ".photo-layer_bottom_block_w .widget.__no-o a[data-module='CommentWidgets'] .widget_count.js-count"

    # SMILIK = '//ul[@class="comments_smiles_lst"]/*/img[contains(@class, "emoji_ok_04")]'

    # @wait_until_send_comment
    def send(self):
        n = self.driver.find_element_by_css_selector(self.NUM_COMMENTS).text

        WebDriverWait(self.driver, 20, 0.1).until(
            EC.element_to_be_clickable((By.XPATH, self.SUBMIT_BTN))
        ).click()

        num_after = str(int(n) + 1)
        WebDriverWait(self.driver, 20, 0.1).until(
            EC.text_to_be_present_in_element((By.CSS_SELECTOR, self.NUM_COMMENTS), num_after)
        )
        # взять количество
        # document.querySelectorAll(
        #     '.photo-layer_bottom_block_w .widget.__no-o a[data-module="CommentWidgets"] .widget_count.js-count')[
        #     0].innerText

    def input_focus(self):
        # time.sleep(1)
        # WebDriverWait(self.driver, 30, 0.1).until(
        #     lambda d: d.find_element_by_xpath(self.INPUT)
        # ).click()

        # после прокрутки к ласт чайлду не нажимать
        WebDriverWait(self.driver, 20, 0.1).until(
            EC.element_to_be_clickable((By.XPATH, self.INPUT))
        ).click()
        # time.sleep(1)

    def input_attach_focus(self):
        self.driver.execute_script("arguments[0].scrollIntoView(true);", self.driver.find_element_by_css_selector(
            '.comments_lst_cnt > div[id^="hook_Block"]:last-child'))



    def input_text(self, element, text):
        WebDriverWait(self.driver, 30, 0.1).until(
            lambda d: d.find_element_by_xpath(element)
        ).send_keys(text)

    # в сенд
    # def submit(self):
    #     self.driver.execute_script('document.querySelector(\'div[class="comments_add-controls"] > button[data-l="t,submit"]\').click()')

    # @wait_until_send_comment
    def submit(self):
        n = self.driver.find_element_by_css_selector(self.NUM_COMMENTS).text

        WebDriverWait(self.driver, 10, 0.1).until(
            EC.element_to_be_clickable((By.XPATH, self.SAVE_BTN))
        ).click()

        num_after = str(int(n) + 1)
        WebDriverWait(self.driver, 10, 0.1).until(
            EC.text_to_be_present_in_element((By.CSS_SELECTOR, self.NUM_COMMENTS), num_after)
        )
        # self.driver.execute_script(
        #     'document.querySelector(\'div[class="comments_add-controls"] > button[data-l="t,submit"]\').click()')

    def go_to_newest_com(self):
        print 'AAAAAAAAAAAAAAAAAAAAAA'
        # comment_added_msg_css = '.comments_form .tip_cnt .tico_img'

        # self.driver.execute_script('document.querySelectorAll(\'.tip.__bot.inverted.tipDiscussion.__hidden\')[0].className = "tip __bot inverted tipDiscussion __active"')
        comment_added_msg_css = '.tip.__bot.inverted.tipDiscussion.__active .tip_cnt'
        WebDriverWait(self.driver, 10, 0.1).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, comment_added_msg_css))
        ).click()
        print 'BBBBBBBBBBBBBBBBBBBBBB'


    def add_comment_text(self, text):
        self.input_focus()
        self.input_text(self.INPUT, text)
        self.send()
        print 'waiting'

    def add_answer_text(self, text):
        # self.input_focus()
        self.input_text(self.INPUT, text)
        self.submit()
        self.go_to_newest_com()

    def click_smile_btn(self):
        # self.driver.find_element_by_xpath(self.SMILE_BTN).click()
        WebDriverWait(self.driver, 20, 0.1).until(
            EC.element_to_be_clickable((By.XPATH, self.SMILE_BTN))
        ).click()

    def click_attach_btn(self):
        WebDriverWait(self.driver, 4, 0.1).until(
            EC.element_to_be_clickable((By.XPATH, self.ATTACH_BTN))
        ).click()
        # self.driver.find_element_by_xpath(self.ATTACH_BTN).click()

    def choose_smile(self, smile_class):
        smile = '//ul[@class="comments_smiles_lst"]/*/img[contains(@class, "' + smile_class + '")]'
        self.click_smile_btn()
        # list of smiles
        WebDriverWait(self.driver, 10, 0.1).until(
            EC.element_to_be_clickable((By.XPATH, self.SMILE_TAB))
        ).click()
        # wait smile
        WebDriverWait(self.driver, 10, 0.1).until(
            EC.element_to_be_clickable((By.XPATH, smile))
        ).click()

    def choose_random_sticker(self):
        self.click_smile_btn()

        # list of stickers
        WebDriverWait(self.driver, 10, 0.1).until(
            EC.element_to_be_clickable((By.XPATH, self.STICKER_TAB))
        ).click()

        print 'OK = ', self.driver.find_element_by_xpath(self.OK_STICKERS_TAB)

        sticker = WebDriverWait(self.driver, 20, 0.1).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, '.comments_smiles_cnt .usmile.__sticker'))
        )
        code = sticker.get_attribute("data-code")
        print code
        sticker.click()
        return code

    def choose_photo(self):
        PHOTO_PC_BTN = '//span[@data-l="t,photo_upload_menu"]'
        PHOTO_PC_INPUT = '//span[@data-l="t,photo_upload_menu"]/*/input[@title="Добавить фото"]'
        self.click_attach_btn()
        # time.sleep(1)

        script_visualise_input = 'document.querySelector(\'span.comments_attach_trigger input[title="Добавить фото"]\').hidden = false'
        print script_visualise_input
        self.driver.execute_script(script_visualise_input)

        PHOTO_PC_INPUT_CSS = 'span.comments_attach_trigger input[title="Добавить фото"]'

        self.driver.find_element_by_css_selector(PHOTO_PC_INPUT_CSS).send_keys(
            os.path.join(os.getcwd(), 'my_test/photo_src/image.jpg'))
        # .find_element_by_xpath(PHOTO_PC_INPUT).send_keys(os.path.join(os.getcwd(), 'my_test/photo_src/image.jpg'))
        # time.sleep(10)

    def click_css(self, css):
        # try:
        # WebDriverWait(self.driver, 30, 0.1).until(
        #     # IndexError: list index out of range
        #     # lambda d: d.find_elements_by_css_selector(css)[0]
        #     lambda d: d.find_element_by_css_selector(css)
        # ).click()
        # except TimeoutException:
        #     print 'cant click on video (?)'
        #     return 0
        WebDriverWait(self.driver, 20, 0.1).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, css))
        ).click()

    def choose_video(self):
        # VIDEOhook_Block_AttachShareVideoMain > av-main-container >
        # data-attach-type = "VIDEOODKL":first-child
        VIDEO_BTN_CSS = 'a[data-l="t,videoLink"]'
        VIDEO_CARD_CSS = '.vid-card_n'

        self.click_attach_btn()

        self.click_css(VIDEO_BTN_CSS)
        self.click_css(VIDEO_CARD_CSS)

    def wait_progress_bar(self):
        # preview_status = 'div[data-id^="PHOTOUPLOADED_8"]'
        progress_bar = '.progress.__bottom.__slim'
        WebDriverWait(self.driver, 20, 0.1).until(
            EC.invisibility_of_element_located((By.CSS_SELECTOR, progress_bar))
        )

    def add_comment_smile(self, smile_class):
        # choose
        self.input_attach_focus()
        self.choose_smile(smile_class)
        self.send()

    def add_comment_sticker(self):
        # choose
        self.input_attach_focus()
        code = self.choose_random_sticker()
        return code

    def add_one_photo(self):
        self.input_attach_focus()
        self.choose_photo()
        # self.wait_loading()

    def add_video(self):
        self.input_attach_focus()
        self.choose_video()
        # self.wait_loading()


# "<div data-id="PHOTOUPLOADED_333:A3enBdlxBa/MHgBW" class="attach-photo_del attachInput"><div class="ic10 ic10_i_close"></div></div>"
# "<div data-id="PHOTOUPLOADED_-1" class="attach-photo_del attachInput"><div class="ic10 ic10_i_close"></div></div>"

class AuthForm(Component):
    LOGIN = '//input[@name="st.email"]'
    PASSWORD = '//input[@name="st.password"]'
    SUBMIT = '//input[@data-l="t,sign_in"]'

    def set_login(self, login):
        # print login
        self.driver.find_element_by_xpath(self.LOGIN).send_keys(login)

    def set_password(self, pwd):
        # print pwd
        self.driver.find_element_by_xpath(self.PASSWORD).send_keys(pwd)

    def submit(self):
        self.driver.find_element_by_xpath(self.SUBMIT).click()
